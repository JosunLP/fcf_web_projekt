<?php // Der index ist die erste Seite der Website,
      // die geladen wird!
      ?>

<?php
require 'tools.php';
$pagename = "Home";

 ?>
<!DOCTYPE html>
<html lang="de" dir="ltr">

<?php

include 'head.php';                                                             // adding the page head

?>
<body>
<?php

include 'header.php';                                                           //adding the header

?>
<div class="content">
    <h1 class="title">Herzlich Willkommen bei be FlixChillFood</h1>
    <p>FCF ist einer Unternehmung der FixToGo Company mit dem Zweck, ein umfassendes alround Packet für Netflix Nutzer zu bieten. Unser Produkt
    findet auf Anfragen passende Packete in Form von passenden Gerichten und/oder Snacks und Merchandise. (Das Produkt befindet sicher derzeit
    in der Alpha Phase, einige Funktionen sind noch nicht Verfügbar)</p>
    <br>
    <p>Für Anregungen und Anfragen wenden sie sich bitte an unsere Support Email unter <a href="mailto:info@FlixChillFood.tool" target="_blank" type="Email">Info@FlixChillFood.tool</a> ,
    oder nutzen sie unsere Social-Media Kanäle.
    <i class="fab fa-youtube"></i>
    <i class="fab fa-facebook"></i>
    <i class="fab fa-twitter"></i>
    </p>
</div>


<?php                                                       //adding the main contend

include 'footer.php';
?>
</body>






</html>
