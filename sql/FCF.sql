DROP DATABASE IF EXISTS `FlixChillFood`;
CREATE DATABASE IF NOT EXISTS `FlixChillFood` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `FlixChillFood`;




###########################################################
#Age_Set###################################################
###########################################################

CREATE TABLE FSK_Table (
	AID int NOT NULL AUTO_INCREMENT,
	FSK int,
	PRIMARY KEY (AID)
	);





###########################################################
#Genres####################################################
###########################################################

CREATE TABLE Genre (
	GID int NOT NULL AUTO_INCREMENT,
	Genre varchar(255),
	PRIMARY KEY (GID)
	);

#############################################################
#Merch#######################################################
#############################################################

CREATE TABLE Merch_Table (
	MEID int ,
	Merch varchar(255),
	MUrl varchar(999),
	Mimg varchar(999),
	PRIMARY KEY (MEID)
	);

###########################################################
#Films_Series##############################################
###########################################################

CREATE TABLE Films (
	FID int NOT NULL AUTO_INCREMENT,
	FTitle varchar(255),
	Film_Link varchar(999),
	Film_img varchar(999),
	GID int,
	AID int,
	MEID int,
	PRIMARY KEY (FID),
	CONSTRAINT FK_GID FOREIGN KEY (GID) REFERENCES Genre(GID),
	CONSTRAINT FK_AID FOREIGN KEY (AID) REFERENCES FSK_Table(AID),
	CONSTRAINT FK_MEID FOREIGN KEY (MEID) REFERENCES Merch_Table(MEID)
	);

CREATE TABLE Series (
	SID int NOT NULL AUTO_INCREMENT,
	STitle varchar(255),
	S_Link varchar(999),
	S_img varchar(999),
	GID int,
	AID int,
	MEID int,
	PRIMARY KEY (SID),
	CONSTRAINT FK_GIDs FOREIGN KEY (GID) REFERENCES Genre(GID),
	CONSTRAINT FK_AIDs FOREIGN KEY (AID) REFERENCES FSK_Table(AID),
	CONSTRAINT FK_MEIDs FOREIGN KEY (MEID) REFERENCES Merch_Table(MEID)
	);


############################################################
#Food_Snacks_and_Drinks#####################################
############################################################

CREATE TABLE Food (
	FOID int NOT NULL AUTO_INCREMENT,
	Food varchar(255),
	Food_Link varchar(999),
	Food_img varchar(999),
	AID int,
	PRIMARY KEY (FOID),
	CONSTRAINT FK_AID_F FOREIGN KEY (AID) REFERENCES FSK_Table(AID)

	);
CREATE TABLE Drinks (
	DID int NOT NULL AUTO_INCREMENT,
	Drink varchar(255),
	Drink_Link varchar(999),
	Drink_img varchar(999),
	AID int,
	PRIMARY KEY (DID),
	CONSTRAINT FK_AID_D FOREIGN KEY (AID) REFERENCES FSK_Table(AID)

	);
CREATE TABLE Snacks (
	SNID int NOT NULL AUTO_INCREMENT,
	Snack varchar (255),
	Snack_Link varchar(999),
	Snack_img varchar(999),
	AID int,
	PRIMARY KEY (SNID),
	CONSTRAINT FK_AID_S FOREIGN KEY (AID) REFERENCES FSK_Table(AID)

	);


###########################################################
#Generator#################################################
###########################################################

CREATE TABLE Generator (
	GENID int NOT NULL AUTO_INCREMENT,
	FID int,
	FOID int,
	DID int,
	MEID int,
	PRIMARY KEY (GENID),
	CONSTRAINT FK_FID_G FOREIGN KEY (FID) REFERENCES Films(FID),
	CONSTRAINT FK_FOID_G FOREIGN KEY (FOID) REFERENCES Food(FOID),
	CONSTRAINT FK_DID_G FOREIGN KEY (DID) REFERENCES Drinks(DID),
	CONSTRAINT FK_MEID_G FOREIGN KEY (MEID) REFERENCES Merch_Table(MEID)
	);

###########################################################
#Users#####################################################
###########################################################

CREATE TABLE Users (
	USID int NOT NULL AUTO_INCREMENT,
	Username varchar (255),
	Firstname varchar (255),
	Lastname varchar (255),
	GENID int,
	AID int,
	PRIMARY KEY (USID),
	CONSTRAINT FK_AID_U FOREIGN KEY (AID) REFERENCES FSK_Table(AID),
	CONSTRAINT FK_GENID_U FOREIGN KEY (GENID) REFERENCES Generator(GENID)

	);


CREATE TABLE passw (
	PWID int NOT NULL AUTO_INCREMENT,
	PSSWD varchar (255),
	USID INT,
	PRIMARY KEY (PWID),
	CONSTRAINT FK_USID_P FOREIGN KEY (USID) REFERENCES Users(USID)

	);




	#Datenbank Injections

	###########################################################
	#FSK Abfrage###############################################
	###########################################################

	#FSK

	INSERT INTO FSK_Table ( FSK)
	VALUES
	( 0),
	( 6),
	( 12),
	( 16),
	( 18)
	;


	###########################################################
	#Genre Abfrage#############################################
	###########################################################


	INSERT INTO Genre (Genre)
	VALUES
	( "Abenteuer"),
	( "Action"),
	( "Horror"),
	( "Sci-Fi")
	;



	###########################################################
	#Merch Abfrage#############################################
	###########################################################

	INSERT INTO Merch_Table (Merch, MUrl, Mimg, MeID)
	VALUES
	("Nothing Found","Nothing Found","Nothing Found",0),
	("Disney-Findet Dorie Mädchen T-Shirt", "https://www.amazon.de/Disney-Findet-Dorie-M%C3%A4dchen-99239-Hellblau/dp/B0748JHP4H/", "img/db_link/merch/dory.jpg", 21),

	("Spreadshirt Asterix & Obelix Idefix Klopft TOC TOC TOC Männer Premium T-Shirt", "https://www.amazon.de/Spreadshirt-Asterix-Premium-T-Shirt-Blaugrau/dp/B01D38IP9S/", "img/db_link/merch/merch.png",22),

	("Kill Bill Herren T-Shirt The Bridge", "https://www.amazon.de/Kill-Bill-T-Shirt-Schwarz-X-Large/dp/B075VWZN1T/", "img/db_link/merch/merch.png",11),

	("FunKo 10459 Star Wars Rogue ONE – Direktor Orson KRENNIC", "https://www.amazon.de/FunKo-10459-Star-Wars-Rogue/dp/B01KJKZ52I/", "img/db_link/merch/merch.png",45),

	("Doctor Who Herren Tardis T-Shirt", "https://www.amazon.de/Doctor-Who-Herren-Tardis-T-shirt/dp/B00L80U7RM/", "img/db_link/merch/merch.png",444),

	("clothinx Damen T-Shirt I am Sherlocked", "https://www.amazon.de/clothinx-Damen-T-Shirt-Sherlocked-Schwarz/dp/B00GUJ6YNC/", "img/db_link/merch/merch.png",233),

	("Lilo & Stitch Adult Girl-Shirt schwarz", "https://www.amazon.de/Lilo-Stitch-Adult-T-Shirt-schwarz/dp/B0796X95K1/", "img/db_link/merch/merch.png",41),

	("Deadpool Dollars T-Shirt Multicolour", "https://www.amazon.de/Deadpool-Dollars-T-Shirt-multicolour-Mehrfarbig/dp/B01CI8P9XY/", "img/db_link/merch/merch.png",17)



	;




	###########################################################
	#Film Abfrage##############################################
	###########################################################


	INSERT INTO Films (FTitle, Film_Link, Film_img, GID, AID, MEID)
	VALUES
	#Abenteuer
	("Findet Dory","https://www.netflix.com/watch/80094319?trackId=13752289&tctx=2%2C0%2C438344f934836ebda1e2bf6f6df9d4ba4085a115%3A1cb7ce4a75ed7d3cc7459338e712541f0675e954%2C%2C", "img/db_link/Films/dory.jpg",2,1,21),

	("Asterix & Obelix","https://www.netflix.com/watch/70242347?trackId=253741134&tctx=11%2C0%2C211482c9-6373-4eae-b1f9-98ac264397ca-61416779%2Cc26d5f9d-a337-4a6a-9bf9-2817ca4e1c87_56954288X28X5343X1544627750636%2Cc26d5f9d-a337-4a6a-9bf9-2817ca4e1c87_ROOT", "img/db_link/Films/asterixobelix.jpg",2,1,22),

	("Kung Fu Panda","https://www.netflix.com/watch/70075480?trackId=13752289&tctx=1%2C2%2C438344f934836ebda1e2bf6f6df9d4ba4085a115%3A1cb7ce4a75ed7d3cc7459338e712541f0675e954%2C%2C", "img/db_link/Films/KungfuPanda.jpg",2,2,0),

	("Die Legende Von Aang","https://www.netflix.com/watch/70119441?trackId=251567757&tctx=8%2C3%2C211482c9-6373-4eae-b1f9-98ac264397ca-61416779%2Cc26d5f9d-a337-4a6a-9bf9-2817ca4e1c87_56954285X28X2298875X1544627750636%2Cc26d5f9d-a337-4a6a-9bf9-2817ca4e1c87_ROOT", "img/db_link/Films/DieLegendeVonAang.jpg",2,2,0),

	("Phantastischen Tierwesen Und Wo Sie Zu Finden Sind","https://www.netflix.com/watch/80111501?trackId=250326522&tctx=2%2C0%2C89a65d99-ff5a-4b80-bc51-0c4da1d1e379-11351776%2C340ba747-3172-4841-a415-9c55e7fdd5d0_56332635X54XX1544626868004%2C340ba747-3172-4841-a415-9c55e7fdd5d0_ROOT", "img/db_link/Films/PhantastischeTierwesen.jpg",2,3,0),

	("Avatar - Aufbruch nach Pandora","https://www.netflix.com/watch/70109892?trackId=13752289&tctx=0%2C2%2C5d8c113c0e5dffbff839890e1532ae405becf4a9%3Aa67b865299c2904dccfd72787c4cf425fd23a7f2%2C%2C", "img/db_link/Films/avatar.jpg",2,3,0),

	("Suicide Squad","https://www.netflix.com/watch/80091595?trackId=13752289&tctx=1%2C0%2C5d8c113c0e5dffbff839890e1532ae405becf4a9%3Aa67b865299c2904dccfd72787c4cf425fd23a7f2%2C%2C", "img/db_link/Films/SuicideSquad.jpg",2,4,0),

	("Django Unchained","https://www.netflix.com/watch/70230640?trackId=13752289&tctx=5%2C2%2C5d8c113c0e5dffbff839890e1532ae405becf4a9%3Aa67b865299c2904dccfd72787c4cf425fd23a7f2%2C%2C", "img/db_link/Films/Django.jpg",2,4,0),

	("IPMan","https://www.netflix.com/watch/70118799?trackId=13752289&tctx=0%2C1%2C6d738edde02f56b8e64f87d8bab2dbdf373e9961%3A91c998a5fb1e7c04d6c68399b66cfb3b29765bd0%2C%2C", "img/db_link/Films/Ipman.jpg",2,5,0),



	#Action
	("Spirit Der Wilde Mustang","https://www.netflix.com/watch/60020810?trackId=13752289&tctx=0%2C0%2Ca75d7ae3-e066-4897-8433-c64d665c916a-489938068%2C%2C", "img/db_link/Films/spirit.jpg",1,1,0),

	("Wickie und die Starken Maener","https://www.netflix.com/watch/80114478?trackId=13752289&tctx=0%2C0%2C188e9b43a1978875d93b1d513bebb5bb14cdb8a3%3A756942ede687b509c9c1ead493de7c8c54ae03f0%2C%2C", "img/db_link/Films/wickie.jpg",1,1,0),

	("Die Unglaublichen","https://www.netflix.com/watch/70001989?trackId=13752289&tctx=15%2C3%2C5303fc2b0a0f449920af49057b6e17ee29f2d54c%3A5ec7d987c146f6ae4a53fe092ffa1119bf766053%2C%2C", "img/db_link/Films/DieUnglaublichen.jpg",1,2,0),

	("Rango","https://www.netflix.com/watch/70137742?trackId=13752289&tctx=34%2C0%2C571d6c39b53d5cda040e00aa01f3628584e2ae9c%3A1ce95024a8c7c95fbd4b6aa4c652d57e8091bf2c%2C%2C", "img/db_link/Films/Rango.jpg",1,2,0),

	("Batman Ninja","https://www.netflix.com/watch/80244455?trackId=253863245&tctx=1%2C2%2C4d8aeae6-2c2b-46fb-8514-e9335f55fa90-73690253%2C68a447af-f3bb-4f97-b724-ddedfda6c2a3_33283899X19XX1545036405319%2C68a447af-f3bb-4f97-b724-ddedfda6c2a3_ROOT", "img/db_link/Films/Batman.jpg",1,3,0),

	("Die Bestimmung: Divergent","https://www.netflix.com/watch/70293461?trackId=253788158&tctx=2%2C1%2C4d8aeae6-2c2b-46fb-8514-e9335f55fa90-73690253%2C68a447af-f3bb-4f97-b724-ddedfda6c2a3_33283900X54XX1545036405319%2C68a447af-f3bb-4f97-b724-ddedfda6c2a3_ROOT", "img/db_link/Films/DieBestimmung.jpg",1,3,0),

	("Deadpool","https://www.netflix.com/watch/80075562?trackId=14170035&tctx=1%2C2%2C557d2e1a-376f-4bd9-9f2b-979ad497f618-487615393%2C89265395-d5a4-4456-95ab-9da6474ebffd_33232323X19XX1545036354559%2C89265395-d5a4-4456-95ab-9da6474ebffd_ROOT", "img/db_link/Films/deadpool.jpg",1,4,17),

	("Red","https://www.netflix.com/watch/70139377?trackId=13752289&tctx=0%2C0%2C3295ac4fba0af739012818d9975abc6eb4dd476e%3A010ac92ed2260476ba4b1c98a7c21e47ee0175c3%2C%2C", "img/db_link/Films/red.jpg",1,4,0),

	("Shooter","https://www.netflix.com/watch/70056430?trackId=13752289&tctx=0%2C0%2Cef36c5c9424f99ae5f21d772b5117cd4288f90b4%3A83346c7cf4fc7be5aebbfd266d67ab8cb4449476%2C%2C", "img/db_link/Films/shooter.jpg",1,5,0),

	("Kill Bill Volume 1","https://www.netflix.com/watch/60031236?trackId=13752289&tctx=22%2C3%2C6d738edde02f56b8e64f87d8bab2dbdf373e9961%3A91c998a5fb1e7c04d6c68399b66cfb3b29765bd0%2C%2C", "img/db_link/Films/killbill.jpg",1,5,11),



	#Horror
	("Little Evil","https://www.netflix.com/watch/80139506?trackId=253932283&tctx=5%2C2%2Ce208942f-c089-47f6-9cf8-8930a1dfeddf-62415576%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_48144123X28X2300069X1544628129328%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_ROOT", "img/db_link/Films/littleevil.jpg",3,3,0),

	("Yoga Hosers","https://www.netflix.com/watch/80098491?trackId=251124987&tctx=12%2C3%2Ce208942f-c089-47f6-9cf8-8930a1dfeddf-62415576%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_48144130X28X7077X1544628129328%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_ROOT", "img/db_link/Films/yogahosers.jpg",3,3,0),

	("Shaun of the Dead","https://www.netflix.com/watch/70003227?trackId=253863245&tctx=1%2C0%2C28f53d5d-df45-41c8-9bed-d338f7e9d0bc-5943714%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_48144119X19XX1544628129328%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_ROOT", "img/db_link/Films/shaundead.jpg",3,4,0),

	("Train to Busan","https://www.netflix.com/watch/80117824?trackId=253863245&tctx=1%2C1%2C28f53d5d-df45-41c8-9bed-d338f7e9d0bc-5943714%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_48144119X19XX1544628129328%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_ROOT", "img/db_link/Films/traintobusan.jpg",3,4,0),

	("Youre Next","https://www.netflix.com/watch/70211045?trackId=251061876&tctx=10%2C2%2Ce208942f-c089-47f6-9cf8-8930a1dfeddf-62415576%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_48144128X29X70104894X1544628129328%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_ROOT", "img/db_link/Films/youarenext.jpg",3,5,0),

	("Apostle","https://www.netflix.com/watch/80158148?trackId=253788158&tctx=2%2C3%2C28f53d5d-df45-41c8-9bed-d338f7e9d0bc-5943714%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_48144120X54XX1544628129328%2Cef556ad4-9d96-4d56-bf55-c9dd2262b05e_ROOT", "img/db_link/Films/apostle.jpg",3,5,0),

	#Sci-Fi
	("Lilo & Stitch","https://www.netflix.com/watch/60022989?trackId=251139816&tctx=5%2C1%2C7e46d8f9-9e64-4bb7-b61f-18e66f6e0adf-62759310%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_11472880X28X561X1544628489039%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_ROOT", "img/db_link/Films/lilostich.jpg",4,1,41),

	("Muppets Aus Dem All","https://www.netflix.com/watch/21633331?trackId=251139816&tctx=5%2C14%2C7e46d8f9-9e64-4bb7-b61f-18e66f6e0adf-62759310%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_11472880X28X561X1544628489039%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_ROOT", "img/db_link/Films/muppets.jpg",4,1,0),

	("Next Gen Das Maedchen Und Ihr Roboter","https://www.netflix.com/watch/80988892?trackId=251139816&tctx=5%2C0%2C7e46d8f9-9e64-4bb7-b61f-18e66f6e0adf-62759310%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_11472880X28X561X1544628489039%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_ROOT", "img/db_link/Films/nextgen.jpg",4,2,0),

	("Monsters Und Aliens","https://www.netflix.com/watch/70102569?trackId=251139816&tctx=5%2C5%2C7e46d8f9-9e64-4bb7-b61f-18e66f6e0adf-62759310%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_11472880X28X561X1544628489039%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_ROOT", "img/db_link/Films/monsterundaliens.jpg",4,2,0),

	("Rogue One: A Star Wars Story","https://www.netflix.com/watch/80108239?trackId=13752289&tctx=0%2C0%2Ccc55606730a04645b67218869da4c64f7c050adc%3A9f101a57d82707cea8cc759f6909bc09a06d2c04%2C%2", "img/db_link/Films/rogueone.jpg",4,3,45),

	("Men In Black","https://www.netflix.com/watch/60001650?trackId=251121952&tctx=6%2C2%2C7e46d8f9-9e64-4bb7-b61f-18e66f6e0adf-62759310%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_11472881X28X89844X1544628489039%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_ROOT", "img/db_link/Films/meninblack.jpg",4,3,0),

	("Bright","https://www.netflix.com/watch/80119234?trackId=253840049&tctx=7%2C0%2Cdd51e9fe-a35b-4613-845a-16230cb86708-63023183%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_11472882X55XX1544628489039%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_ROOT", "img/db_link/Films/bright.jpg",4,4,0),

	("Ausloeschung","https://www.netflix.com/watch/80206300?trackId=253788158&tctx=2%2C5%2Cdd51e9fe-a35b-4613-845a-16230cb86708-63023183%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_11472877X54XX1544628489039%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_ROOT", "img/db_link/Films/aus.jpg",4,4,0),

	("Death Race","https://www.netflix.com/watch/70098610?trackId=253779008&tctx=4%2C30%2C7e46d8f9-9e64-4bb7-b61f-18e66f6e0adf-62759310%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_11472879X28X43048X1544628489039%2Cb90b2f33-2ee8-4fcc-8303-80e28a1953c7_ROOT", "img/db_link/Films/deathrace.jpg",4,5,0),

	("Gantz:O","https://www.netflix.com/watch/80149259?trackId=13752289&tctx=41%2C0%2Cc1da943d19c54a3d54ad0d452bb6684d1ea912f7%3A77afe536e0cfc43c0b3b74a67de2c5da2c3c81b4%2C%2C", "img/db_link/Films/gantz.jpg",4,5,0)


	;

	INSERT INTO Series (STitle, S_Link, S_img, GID, AID, MeID)
	VALUES
	#Abenteuer

	("Lemony Snickets: A Series of Unfortunate Events","https://www.netflix.com/watch/80114991?trackId=252072904&tctx=0%2C0%2C623a8053-3b4a-4689-850a-b6c44277c73d-207681467%2C21605bf9-d030-4521-b283-1ae32fa952de_51439232X20XX1546517816169%2C21605bf9-d030-4521-b283-1ae32fa952de_ROOT", "img/db_link/Series/serie.png",2,2,0),

	("Sherlock","https://www.netflix.com/watch/70174779?trackId=200257859", "img/db_link/Series/serie.png",2,3,233),

	("The End of the F***ing World","https://www.netflix.com/watch/80175723?trackId=250321472&tctx=11%2C3%2C27ca7594-6034-4b64-98be-eccc829978f4-196064249%2C21605bf9-d030-4521-b283-1ae32fa952de_51439243X9XX1546517816169%2C21605bf9-d030-4521-b283-1ae32fa952de_ROOT", "img/db_link/Series/serie.png",2,4,0),




	#Action

	("Troll Jaeger","https://www.netflix.com/watch/80075918?tctx=3%2C0%2C899bf49b-a76c-4ee6-a3e8-1dc45ac85841-191622966%2C%2C", "img/db_link/Series/serie.png",1,2,0),

	("Brooklyn Nine-Nine","https://www.netflix.com/watch/80004161?trackId=250350385&tctx=2%2C2%2C623a8053-3b4a-4689-850a-b6c44277c73d-207681467%2C21605bf9-d030-4521-b283-1ae32fa952de_51439234X54XX1546517816169%2C21605bf9-d030-4521-b283-1ae32fa952de_ROOT", "img/db_link/Series/serie.png",1,3,0),

	("The Protector","https://www.netflix.com/watch/80189588?trackId=250314597&tctx=0%2C0%2Cb72ebc6b-296b-4b05-b78e-cd5b7ba21b59-93527171%2C21605bf9-d030-4521-b283-1ae32fa952de_51439233X19XX1546517816169%2C21605bf9-d030-4521-b283-1ae32fa952de_ROOT", "img/db_link/Series/serie.png",1,4,0),



	#Horror

	("Little Evil","https://www.netflix.com/watch/80230071?trackId=200257859", "img/db_link/Series/serie.png",3,4,0),

	("American Horror Story","https://www.netflix.com/watch/70260290?trackId=13752289&tctx=0%2C0%2Ca7aff8c199915e54c1ef099b011bf8fe0cdc6864%3Aae9ed407726cf4a10b793a672aa5db764f8225fe%2C%2C", "img/db_link/Series/serie.png",3,5,0),




	#Sci-Fi

	("Mob Psycho","https://www.netflix.com/watch/80213342?trackId=251480758&tctx=3%2C1%2Ccbe55b42-8ee7-45bd-ac70-6b45baaf05a0-1853803%2C96244e4b-d4ea-4263-bd69-0ba02a2f560c_45287784X28X1192490X1546518549981%2C96244e4b-d4ea-4263-bd69-0ba02a2f560c_ROOT", "img/db_link/Series/serie.png",4,3,0),

	("Doctor Who","https://www.netflix.com/watch/70174742?trackId=200257859", "img/db_link/Series/serie.png",4,4,444),

	("Van Helsing","https://www.netflix.com/watch/80126264?trackId=253417989&tctx=9%2C2%2Cd89454d7-27e0-4af1-969a-ea08503c6571-218532408%2C96244e4b-d4ea-4263-bd69-0ba02a2f560c_45287790X29X80223989X1546518549981%2C96244e4b-d4ea-4263-bd69-0ba02a2f560c_ROOT", "img/db_link/Series/serie.png",4,5,0)



	;


	###########################################################
	#Food_Drink_Snacks#########################################
	###########################################################



	INSERT INTO Food (Food, Food_Link, Food_img, AID)
	VALUES
	("Pizza", "#", "img/db_link/Food/Food.png",2),

	("Döner", "#", "img/db_link/Food/Food.png",2),

	("Borito", "#", "img/db_link/Food/Food.png",2),

	("Knobi Baguett", "#", "img/db_link/Food/Food.png",2),

	("Croque", "#", "img/db_link/Food/Food.png",2),

	("Nuggets", "#", "img/db_link/Food/Food.png",2)

	;

	INSERT INTO Drinks (Drink, Drink_Link, Drink_img, AID)
	VALUES
	("Coca-Cola", "#", "img/db_link/Drinks/drink.png",2),

	("Sprite", "#", "img/db_link/Drinks/drink.png",2),

	("Fanta", "#", "img/db_link/Drinks/drink.png",2),

	("Bier", "#", "img/db_link/Drinks/drink.png",4),

	("Rotwein", "#", "img/db_link/Drinks/drink.png",4),

	("Long Island Ice Tea", "#", "img/db_link/Drinks/drink.png",5),

	("Gin Tonic", "#", "img/db_link/Drinks/drink.png",5),

	("Relentless Energy", "#", "img/db_link/Drinks/drink.png",4)

	;


	INSERT INTO Snacks (Snack, Snack_Link, Snack_img, AID)
	VALUES
	("Pop-Corn", "#", "img/db_link/Snacks/snack.png",2),

	("Nachos", "#", "img/db_link/Snacks/snack.png",2),

	("Chips", "#", "img/db_link/Snacks/snack.png",2),

	("Flips", "#", "img/db_link/Snacks/snack.png",2),

	("Kikos", "#", "img/db_link/Snacks/snack.png",2)

	;



	###########################################################
	#Admin User################################################
	###########################################################

	INSERT INTO Generator (GENID)
	VALUES (1);

	INSERT INTO Users ( Username, Firstname, Lastname, GENID, AID)
	VALUES ( "Admin", "Admin", "User", 1, 5);

	INSERT INTO passw ( PSSWD, USID)
	VALUES ( "root", 1);
