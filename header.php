<?php   // Hier deffinieren wir unseren
        // Page Header, also unter anderem unser
        // Menue
        $acc = new AccountUsage;

 ?>



<nav class="navbar navbar-expand-lg navbar-light">
  <img src="img/logo/logo.png" alt="logo" class="logo"/>
  <a class="navbar-brand" href="index.php">FlixChillFood | [<?php echo $pagename; ?>] </a>
  <button class="navbar-toggler navbar-toggler-icon" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"></button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <?php $indexname1 = "Home"; ?>
        <a class="nav-link <?php if ($pagename == $indexname1) {echo 'active';}?> " href="index.php">Home <?php if ($pagename == $indexname1) {echo "<span class='sr-only'>(current)</span>";}?></a>
      </li>
      <li class="nav-item">
        <?php $indexname2 = "Produkte"; ?>
        <a class="nav-link <?php if ($pagename == $indexname2) {echo 'active';}?>" href="produkte.page.php" disabled>Produkte <?php if ($pagename == $indexname2) {echo "<span class='sr-only'>(current)</span>";}?></a>
      </li>
      <li class="nav-item">
        <?php $indexname3 = "Suchen"; ?>
        <a class="nav-link <?php if ($pagename == $indexname3) {echo 'active';}?>" href="search.page.php" disabled>Suchen <?php if ($pagename == $indexname3) {echo "<span class='sr-only'>(current)</span>";}?></a>
      </li>
      <li class="nav-item">
        <?php $indexname4 = "Account erstellen"; ?>
        <a class="nav-link <?php if ($pagename == $indexname4) {echo 'active';}?>" href="signin.page.php" disabled>Account erstellen <?php if ($pagename == $indexname4) {echo "<span class='sr-only'>(current)</span>";}?></a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0 search " method="get" target="_self">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="sg" disabled>
      <button class="btn btn-primary my-2 my-sm-0" type="submit" disabled>Search</button>
    </form>
    <?php
    //$ps = new SearchEngine;
    //$sg = $ps -> GetChecker("sg");
    //$ps -> SearchQuest($sg, $db_link); ?>
  </div>
</nav>
