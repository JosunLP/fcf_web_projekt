<?php // Hier definieren wir unseren
      // Page Head, wo unter anderem das Charset
      // und der Titel definiert werden

  ?>


<head>
  <meta charset="utf-8">
  <link rel="icon" href="img/fav.png">
  <title>FlixChillFood <?php echo ' | '. $pagename; ?></title>
  <script src="js/jquery-3.3.1.min.js"></script> <!-- Adding JQuery -->
  <script src="js/popper.min.js"></script> <!-- Adding Popper.js Framework -->
  <script src="js/bootstrap.min.js"></script> <!-- Adding Bootstrap Framework -->
  <script src="js/tools.js"></script> <!-- Adding scripts -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"><!-- Adding Font Awesome -->
  <link rel="stylesheet" href="css/bootstrap.min.css" >
  <link rel="stylesheet" href="css/font.css"> <!-- Adding Font CSS -->
  <link rel="stylesheet" href="css/root.css"> <!-- Adding ROOT CSS -->
  <link rel="stylesheet" href="css/main.css"> <!-- Adding Main CSS -->
</head>
