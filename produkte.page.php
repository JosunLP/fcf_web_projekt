<?php
require 'tools.php';
$pagename = "Produkte";//##### Hier wird der Seitentitel definiert ######

?>
<!DOCTYPE html>
<html lang="de" dir="ltr">

<?php

include 'head.php';

?>
<body>
<?php

include 'header.php';
?>

<div class="content">

    <h1 class="title"><?php echo $pagename ?></h1>
    <div class="product_galerie_output">
      <h2>Filme</h2>


      <?php
      $g = new GaleryBuilder;
      $g -> AgeGalery($db_link, 'films');
      ?>


    </div>
    <div class="product_galerie_output">
      <h2>Serien</h2>


      <?php
      $g -> AgeGalery($db_link, 'series');
      ?>


    </div>



</div>

<?php
include 'footer.php';
?>
</body>






</html>
