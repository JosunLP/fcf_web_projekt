<?php
require 'tools.php';
$pagename = "Suchen";//##### Hier wird der Seitentitel definiert ######

?>
<!DOCTYPE html>
<html lang="de" dir="ltr">

<?php

include 'head.php';

?>
<body>
<?php

include 'header.php';
?>

<div class="content">

    <h1 class="title"><?php echo $pagename ?></h1>

    <form class="order formular" target="_self" method="get">
      <fieldset>
        <legend>Deine Bestellung: </legend>
        <div class="form-group">
          <label for="Inputtype">Dein Produkttyp</label>
          <select class="form-control" id="typeFormControlSelect" name="type" required>
            <option name="Films">Films<?php //Tabelle deffinieren  ?></option>
            <option name="Series">Series<?php //Tabelle deffinieren  ?></option>
            <option name="Snacks">Snacks<?php //Tabelle deffinieren  ?></option>
            <option name="Food">Food<?php //Tabelle deffinieren  ?></option>
          </select>
          <small id="typeHelp" class="form-text text-muted">Entscheide dich hier für die Art des Produktes</small>
        </div>
        <div class="form-group">
          <label for="InputName">Produkt Name</label>
          <input type="text" class="form-control" id="InputName" name="name" placeholder="Product name" required>
          <small id="nameHelp" class="form-text text-muted">Schreibe den Namen deines Produktes</small>
        </div>
        <div class="form-check">
          <input type="checkbox" class="form-check-input" name="check" id="Check" required>
          <label class="form-check-label" for="Check">Ich bin mit den <a href="termsofuse.page.php">Nutzungsbedingungen</a> einverstanden!</label>
        </div>
        <button type="submit" class="btn btn-primary button">Bestätigen</button>
      </fieldset>

    </form>
    <div class="product_galerie_output">
      <?php
      $order = new SearchEngine;
      $get_tp = $order -> GetChecker("type");
      $get_nm = $order -> GetChecker("name");
      $get_ck = $order -> GetChecker("check");
      $order -> FormSearch($get_tp, $get_nm, $get_ck, $db_link);
       ?>

    </div>


</div>

<?php
include 'footer.php';
?>
</body>






</html>
