<?php
require 'tools.php';
$pagename = "Account erstellen";//##### Hier wird der Seitentitel definiert ######

?>
<!DOCTYPE html>
<html lang="de" dir="ltr">

<?php

include 'head.php';

?>
<body>
<?php

include 'header.php';
?>

<div class="content">

    <h1 class="title"><?php echo $pagename ?></h1>

    <form class="formular" method="post">
      <div class="form-group">
        <label for="username">Username</label>
        <input type="text" name="username" class="form-control" id="InputUsername" aria-describedby="userName" placeholder="Enter Username">
      </div>
      <div class="form-group">
        <label for="firstName">Vorname</label>
        <input type="text" name="firstName" class="form-control" id="InputFirstName" aria-describedby="firstName" placeholder="Enter first Name">
      </div>
      <div class="form-group">
        <label for="lastName">Nachname</label>
        <input type="text" name="lastName" class="form-control" id="InputLastName" aria-describedby="lastName" placeholder="Enter last Name">
      </div>
      <div class="form-group">
        <label for="age">Alter</label>
        <input type="number" name="age" class="form-control" id="InputAge" aria-describedby="age" placeholder="Enter Age">
      </div>
      <div class="form-group">
        <label for="InputPassword">Password</label>
        <input type="password" name="pw" class="form-control" id="InputPassword" placeholder="Enter Password">
        <small id="password" class="form-text text-muted">Bitte achte darauf, dass dein Passwort mindestens 10 Zeichen lang ist!</small>
      </div>
      <div class="form-check">
        <input type="checkbox" class="form-check-input" name="check" id="Check" required>
        <label class="form-check-label" for="Check">Ich bin mit den <a href="termsofuse.page.php">Nutzungsbedingungen</a> einverstanden!</label>
      </div>
      <button type="submit" class="btn btn-success button">Submit</button>
    </form>

    <?php
    $seg = new SearchEngine;
    $uname_proof = $seg -> PostChecker("username");
    $fname_proof = $seg -> PostChecker("firstName");
    $lname_proof = $seg -> PostChecker("lastName");
    $age_proof = $seg -> PostChecker("age");
    $pw_proof = $seg -> PostChecker("pw");
    $check_proof = $seg -> PostChecker("check");
    $acc -> AddUser($db_link, $uname_proof, $fname_proof, $lname_proof, $age_proof, $pw_proof, $check_proof);
     ?>


</div>

<?php
include 'footer.php';
?>
</body>






</html>
