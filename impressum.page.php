<?php
require 'tools.php';
$pagename = "Impressum";//##### Hier wird der Seitentitel definiert ######

?>
<!DOCTYPE html>
<html lang="de" dir="ltr">

<?php

include 'head.php';

?>
<body>
<?php

include 'header.php';
?>

<div class="content">

    <h1 class="title"><?php echo $pagename ?></h1>

    <h2>Angaben gem&auml;&szlig; &sect; 5 TMG</h2> <p>Max Mustermann<br /> Musterstra&szlig;e 111<br /> Geb&auml;ude 44<br /> 90210 Musterstadt</p>
    <h2>Kontakt</h2> <p>Telefon: +49 (0) 123 44 55 66<br /> Telefax: +49 (0) 123 44 55 99<br /> E-Mail: mustermann@musterfirma.de</p>
    <h2>Verantwortlich f&uuml;r den Inhalt nach &sect; 55 Abs. 2 RStV</h2> <p>Beate Beispielhaft<br /> Musterstra&szlig;e 110<br /> Geb&auml;ude 33<br /> 90210 Musterstadt</p>
    <h3>Haftung f&uuml;r Inhalte</h3> <p>Als Diensteanbieter sind wir gem&auml;&szlig; &sect; 7 Abs.1 TMG f&uuml;r eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach &sect;&sect; 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, &uuml;bermittelte oder gespeicherte fremde Informationen zu &uuml;berwachen oder nach Umst&auml;nden zu forschen, die auf eine rechtswidrige T&auml;tigkeit hinweisen.</p> <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unber&uuml;hrt. Eine diesbez&uuml;gliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m&ouml;glich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p> <h3>Haftung f&uuml;r Links</h3> <p>Unser Angebot enth&auml;lt Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb k&ouml;nnen wir f&uuml;r diese fremden Inhalte auch keine Gew&auml;hr &uuml;bernehmen. F&uuml;r die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf m&ouml;gliche Rechtsverst&ouml;&szlig;e &uuml;berpr&uuml;ft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.</p> <p>Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>


</div>

<?php
include 'footer.php';
?>
</body>






</html>
